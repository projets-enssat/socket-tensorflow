(function () {
    /*
    compatibility
    */
    navigator.getMedia = (navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia);

    /*
    Load elements references
    */
    const video = document.getElementById('webcam-preview');
    const canvas = document.getElementById('snapshot-preview');
    const image = document.getElementById('classified-image');
    const results = document.getElementById('results');
    const startml = document.getElementById('start-ml');

    /*
    functions
    */
    function sendpicture() {
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;

        const context = canvas.getContext('2d');
        context.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
        let b64image = canvas.toDataURL('image/png');

        socket.emit('photo', { image: b64image });
        console.log('picture sent to ml server');
    }



    function showPredictions(items) {
        let rows = items.map((item) => {
            return `
            <tr>
                <td>${item.className}</td>
                <td>${item.probability}</td>
            </tr>
            `
        }).join('');

        results.innerHTML = `
        <table>
            <thead>
                <tr>
                    <th>Class</th>
                    <th>Proba.</th>
                </tr>
            </thead>
            <tbody>
                ${rows}
            </tbody>
        </table>`;
    }

    /*
    bindings
    */
    navigator.getMedia(
        {
            video: true,
            audio: false
        },
        function (stream) {
            if (navigator.mozGetUserMedia) {
                video.mozSrcObject = stream;
            } else {
                video.srcObject = stream;//vendorURL.createObjectURL(stream);
            }
            video.play();
        },
        function (err) {
            console.log("An error occured! " + err);
        }
    );

    startml.addEventListener('click', sendpicture);

    let socket = io.connect();

    /* Not very convenient as server is slow to perform ml.
    socket.on('connect', () => {
        // after socket connection is opened, send webcam picture each second
        setInterval(() => {
            sendpicture();
        }, 1000);
    })
    */

    socket.on('classification', (classification) => {
        console.log('classification received', classification);
        image.src = classification.image;
        showPredictions(classification.predictions);
    })

    socket.on('classificationerror', (message) => {
        alert(message);
    })

})()