const express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
require('./classify');

app.use(express.static('public'));

io.on('connection', function (socket) {
    console.log('a user connected');

    // called when image classification is done
    function onImageClassified(image, predictions) {
        console.log('Classification Results:', predictions);
        socket.emit('classification', {
            image: image,
            predictions: predictions
        });
    }

    // callbacks
    function onClassificationError(message) {
        socket.emit('classificationerror', message);
    }

    socket.on('photo', (data) => {
        classifyImage(data.image, onImageClassified, onClassificationError);
    })
});

http.listen(3000, () => { console.log('listening on *:3000') });
