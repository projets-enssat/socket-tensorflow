# Webcam images classification

When user click on the button, image data is sent to the server (using socket.io) which use a tensorflow model to classify detected object. Results are then sent back via an event and displayed in the browser.

## Usage

This project is a nodejs server. To use it, first, install dependencies :
```bash
npm install
```

Then run express server this way
```bash
node server.js
```

The web service will be available at `http://localhost:3000`

## Examples

### Example 1: coffee mug
![example1: coffee mug](docs/example1_mug.png)

### Example 2: smartphone
![example2: smartphone](docs/example2_phone.png)