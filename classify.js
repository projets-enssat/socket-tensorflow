const mobilenet = require('@tensorflow-models/mobilenet');
const tfnode = require('@tensorflow/tfjs-node');

// classifier lock
let locked = false;


const readImage = b64image => {
    // remove data:image/png;base64, header
    const b64data = b64image.replace(/^data:image\/\w+;base64,/, "");

    // convert b64data as buffer
    const buffer = new Buffer.from(b64data, 'base64');

    // generating tfimage from buffer
    return tfnode.node.decodeImage(buffer, 3);
}


classifyImage = async (b64image, callback, errorCallback) => {
    // 1 classification at a time as it is very expensive in GPU
    if (!locked) {
        locked = true;

        console.log('classification start');
        const image = readImage(b64image);
        const mobilenetModel = await mobilenet.load();
        const predictions = await mobilenetModel.classify(image);
        // send data to callback function
        callback(b64image, predictions);

        locked = false;
    } else {
        errorCallback("The server is already processing another image");
    }
}

module.exports = classifyImage;
